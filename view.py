from __future__ import division, print_function, unicode_literals
import random
from cocos.sprite import Sprite
from sprites import PlayerSprite
from pyglet.gl import *
from cocos.layer import Layer, ColorLayer
from cocos.scene import Scene
from cocos.director import director
from cocos.actions import *
from constants import *
import soundex



__all__ = ['get_newgame']

class GameView( Layer ):
    is_event_handler = True

    def __init__(self):
        super(GameView,self).__init__()
        from main import BubbleLayer

        self.poland_lines = [
            'Polska stronk!',
            'To the\nmoon!',
            'Kurwa!',
            'PSHE! PSH!',
            'UP! UP UP!',
            'FLAP!',
            'FLAP-FLAP!',
            'ALMOST DONE!',
            '~(._.)~',
            'INTO\nSPACE!',
            'POLAND\nCAN!',
            'ALMOST!',
            'FLY!',
            'I WANT!',
            'WHEEEE!',
        ]

        self.crowd_lines = [
            'Poland,\nstahp!',
            'Noooo!',
            'Stop\nright\nnow!',
            'MMMMM!',
            'A-A-A-A\nAHHHH!!!',
            'Poland\nplz!',
            '\(x_x)/',
            'Poland!',
            'Please!',
            'HEY YOU!',
            'Stop plz!',
            'Enough!',
            'You\'re\nbreaking\neverything!',
        ]

        width, height = director.get_window_size()
        self.window_width, self.window_height = width, height

        aspect = width / float(height)
        self.grid_size = ( int(20 *aspect),20)
        self.duration = 8

        self.player = PlayerSprite(position=(width/2, 390))
        self.earth = Sprite('earth.png', position=(width/2, -200))
        self.add(self.earth)
        self.add(self.player)

        self.player.bubble = BubbleLayer()
        self.player.add(self.player.bubble)

        self.bubbles = BubbleLayer()
        self.add(self.bubbles)
        self.player.bubble.show_bubble('W,A,S,D :)', delay=5)

        shake_part = MoveBy((-1.0, -10.0), 0.05)
        self.shake = shake_part + Reverse(shake_part)*2 + shake_part

    def on_key_press (self, key, modifiers):
        if random.random() > 0.5:
            self.player.bubble.show_bubble(random.choice(self.poland_lines))

        if key in [97, 65361]:
            self.player.animate('jump', 'l')
            self.earth.do(RotateBy(30, .3)+ CallFunc(lambda: self.player.animate('stay', self.player.direction)))
        elif key in [100, 65363]:
            self.player.animate('jump', 'r')
            self.earth.do(RotateBy(-30, .3)+ CallFunc(lambda: self.player.animate('stay', self.player.direction)))
        if key in [32, 119]:
            soundex.play(random.choice(['jump_1.mp3','jump_2.mp3','jump_3.mp3']))
            self.player.do(JumpBy( (0,0), 150,1,.5) + \
                           CallFunc(lambda: self.player.animate('stay', self.player.direction)) + \
                           CallFunc(lambda: self.earth.do(self.shake))
            )
            if random.random() > 0.5 or random.random() <= 0.1:
                self.bubbles.show_bubble(random.choice(self.crowd_lines), bubble_image='bubble_2.png')
            if random.random() < 0.5 or random.random() >= 0.9:
                self.bubbles.show_bubble(random.choice(self.crowd_lines), bubble_image='bubble_3.png')

    def on_enter(self):
        super(GameView,self).on_enter()
        soundex.play_music()

    def on_exit(self):
        super(GameView,self).on_exit()


def get_newgame():
    from main import BackgroundLayer

    soundex.stop_music(soundex.music_player)
    soundex.music_player.volume = soundex.music_player.volume
    soundex.current_music = None
    soundex.set_music('crowd.mp3', player=soundex.music_player)

    scene = Scene()
    view = GameView()

    scene.add(BackgroundLayer('bg_game.png'), z=0, name="background")
    scene.add(view, z=3, name="view")

    fadeable = ColorLayer(0,0,0,255)
    fadeable.do(FadeOut(1.5))
    scene.add(fadeable, z=2)

    return scene
