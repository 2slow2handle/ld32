from __future__ import division, print_function, unicode_literals
import os
import pyglet
from cocos.sprite import Sprite
from constants import RESOURCE_FOLDER
from cocos.rect import Rect


class AnimatedSprite(Sprite):

    def __init__(self, *args, **kwargs):
        self.animations = {}
        self.state = None
        self.direction = None
        self.add_animations()
        if self.animations:
            super(AnimatedSprite, self).__init__(self.animations[(self.state, self.direction)][0], *args, **kwargs)

    def add_animations(self):
        # abstract
        pass

    def add_animation(self, filename, name, direction, dimensions, delay):
        filename=os.path.join(RESOURCE_FOLDER, filename)
        sheet = pyglet.image.load(filename)
        sequence = pyglet.image.ImageGrid(sheet, *dimensions)
        self.animations[(name, direction)] = [pyglet.image.Animation.from_image_sequence(sequence, delay, True), (0, 0), 1]

    def animate(self, name, direction):
        self.image = self.animations[(name, direction)][0]
        self.state = name
        self.direction = direction
        
        
class PlayerSprite(AnimatedSprite):

    def add_animations(self):
        self.state = 'stay'
        self.direction = 'r'
        self.add_animation('jump_r.png', 'jump', 'r', (1, 4), .1)
        self.add_animation('jump_l.png', 'jump', 'l', (1, 4), .1)
        self.add_animation('stay_r.png', 'stay', 'r', (1, 1), 1)
        self.add_animation('stay_l.png', 'stay', 'l', (1, 1), 1)