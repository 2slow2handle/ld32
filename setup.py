from cx_Freeze import setup, Executable
import os

includefiles = ['avbin.dll']
for (dirpath, dirnames, filenames) in os.walk("data"):
    includefiles.extend([os.path.join(dirpath, name) for name in filenames])


zip_includes = [('.\\venv\\Lib\\site-packages\\cocos\\resources\\fire.png', 'cocos\\resources\\fire.png' )]
includes = []
excludes = ['Tkinter']
packages = []

setup(
    name = 'Poland space program - 32th Ludum Dare compo game',
    version = '0.1',
    description = 'Poland space program',
    author = '2slow2handle',
    author_email = '2slow2handle@gmail.com',
    options = {'build_exe': {'excludes':excludes,
                             'packages':packages,
                             'include_files':includefiles,
                             'zip_includes': zip_includes,

                             'compressed':True,
                             'copy_dependent_files':True,
                             'create_shared_zip':False,
                             'include_in_shared_zip':False,
                             'optimize':2,
                             'icon':'icon.ico',
                }},
    executables = [Executable(script='main.py',
                              base = 'Win32GUI',
                              compress = True,
                              appendScriptToLibrary = False,
                              appendScriptToExe = True,
                              replacePaths = [('*', '')],
                   )]
)