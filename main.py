from __future__ import division, print_function, unicode_literals

from cocos.layer import *
from cocos.scene import Scene
from cocos.scenes.transitions import *
from cocos.actions import *
from cocos.sprite import *
from cocos.menu import *
from cocos.text import *
from constants import *
import pyglet
from pyglet import gl, font
from pyglet.window import key

import soundex
import random
import os
from shared import global_state


class OptionsMenu( Menu ):
    def __init__(self):
        super( OptionsMenu, self).__init__('')

        self.font_title['font_name'] = FONT_TITLE
        self.font_title['font_size'] = 62
        self.font_title['color'] = (0,0,0,255)

        self.font_item['font_name'] = FONT_TITLE
        self.font_item['color'] = (0,0,0,255)
        self.font_item['font_size'] = 48
        self.font_item_selected['font_name'] = FONT_TITLE
        self.font_item_selected['color'] = (16,255,32,255)
        self.font_item_selected['font_size'] = 48

        # example: menus can be vertical aligned and horizontal aligned
        self.menu_anchor_y = CENTER
        self.menu_anchor_x = CENTER

        items = []

        self.volumes = ['MUTE','10','20','30','40','50','60','70','80','90','100']

        items.append( ToggleMenuItem('SHOW FPS: ', self.on_show_fps, director.show_FPS) )
        items.append( MenuItem('FULLSCREEN', self.on_fullscreen) )
        items.append( MenuItem('BACK', self.on_quit) )
        self.create_menu( items )

    def on_fullscreen( self ):
        director.window.set_fullscreen( not director.window.fullscreen )

    def on_quit( self ):
        self.parent.switch_to( 0 )

    def on_show_fps( self, value ):
        director.show_FPS = value

    def on_sfx_volume( self, idx ):
        vol = idx / 10.0
        soundex.sound_volume( vol )

    def on_music_volume( self, idx ):
        vol = idx / 10.0
        soundex.music_volume( vol )

    def on_flip_3d( self, value ):
        global_state.flip_3d = value


class MainMenu( Menu ):

    def __init__(self):
        super( MainMenu, self).__init__('')

        self.select_sound = soundex.load('pok.mp3')

        self.font_item['font_name'] = FONT_TITLE
        self.font_item['color'] = (0,0,0,255)
        self.font_item['font_size'] = 48
        self.font_item_selected['font_name'] = FONT_TITLE
        self.font_item_selected['color'] = (16,255,32,255)
        self.font_item_selected['font_size'] = 48

        self.menu_anchor_y = CENTER
        self.menu_anchor_x = CENTER

        items = []

        items.append( MenuItem('New game', self.on_new_game) )
        items.append( MenuItem('Options', self.on_options) )
        # items.append( MenuItem('Scores', self.on_scores) )
        items.append( MenuItem('Exit', self.on_quit) )

        # self.create_menu( items, shake(), shake_back() )
        self.create_menu( items )

    def on_new_game(self):
        import view
        director.push(FadeTRTransition(view.get_newgame(), 1.5 ))

    def on_options( self ):
        self.parent.switch_to(1)

    def on_scores( self ):
        self.parent.switch_to(2)

    def on_quit(self):
        pyglet.app.exit()


class TextLayer(Layer):

    def __init__(self):
        super(TextLayer, self).__init__()
        w, h = director.get_window_size()
        label = Label("I've upload this to be able to vote\nSorry about that (._.)\"",
            font_name=FONT_TITLE,
            color=(0,0,0,255),
            font_size=14,
            width=500,
            anchor_x='left',
            anchor_y='center',
            multiline=True)

        label.position = 30, 120
        self.add(label)

        label = Label('LD 32 COMPO / HATE-MARINA\n2SLOW2HANDLE@GMAIL.COM',
            font_name=FONT_TITLE,
            color=(0,0,0,255),
            font_size=14,
            width=360,
            anchor_x='left',
            anchor_y='center',
            multiline=True)

        label.position = 800, 30
        self.add(label)

        label = Label('      POLAND\nSPACE PROGRAM',
            font_name=FONT_TITLE,
            color=(255,16,32,255),
            font_size=48,
            width=666,
            anchor_x='center',
            anchor_y='center',
            multiline=True)

        label.position = w/2, h-100
        self.add(label)


class BubbleLayer( Layer ):

    def __init__(self):
        self.bubbles = []
        super(BubbleLayer, self).__init__()

    def show_bubble(self, text, bubble_image='bubble_1.png', position=(0,0), callback=None, delay=1, animate=True):

        for bubble in self.bubbles[:]:
            if not bubble.visible:
                self.remove(bubble)
                self.bubbles.remove(bubble)

        bubble = Sprite(bubble_image)
        bubble.anchor_x = 0
        bubble.anchor_Y = 0

        if bubble_image == 'bubble_1.png':
            bubble.position = (300, 100)
            msg = Label(text,
                        font_size=36,
                        color=(0,0,0,255),
                        font_name=FONT_TITLE,
                        anchor_y='center',
                        anchor_x='center',
                        multiline=True,
                        width=bubble.width)
            msg.position = (100, 0)
            bubble.add(msg)

        if bubble_image == 'bubble_2.png':
            bubble.position = (random.randint(800, 1000), random.randint(100, 300))
            msg = Label(text,
                        font_size=20,
                        color=(0,0,0,255),
                        font_name=FONT_TITLE,
                        anchor_y='center',
                        anchor_x='center',
                        multiline=True,
                        width=bubble.width)
            bubble.add(msg)
            msg.position = (100, 0)

        if bubble_image == 'bubble_3.png':
            bubble.position = (random.randint(300, 500), random.randint(200, 350))
            msg = Label(text,
                        font_size=14,
                        color=(0,0,0,255),
                        font_name=FONT_TITLE,
                        anchor_y='center',
                        anchor_x='center',
                        multiline=True,
                        width=bubble.width)
            bubble.add(msg)
            msg.position = (50, 20)



        actions = FadeIn(.3) + \
                  CallFunc(lambda: msg.do(Show())) + \
                  Delay(delay) +  \
                  FadeOut(.3) + \
                  CallFunc(lambda: msg.do(Hide())) + \
                  CallFunc(lambda: bubble.do(Hide()))

        if callback:
            actions += CallFunc(callback)

        self.bubbles.append(bubble)
        self.add(bubble)
        bubble.do(actions)


class BackgroundLayer(Layer):
    def __init__( self, path_name ):
        super(BackgroundLayer, self).__init__()
        #self.image = pyglet.resource.image('bg_menu.jpg') # raises exception @ tiles.py, line 558
        self.image = image.load(os.path.join(RESOURCE_FOLDER, path_name))

    def draw( self ):
        glPushMatrix()
        self.transform()
        self.image.blit(0,0)
        glPopMatrix()


if __name__ == "__main__":

    from shared import keyboard

    pyglet.resource.path.append(RESOURCE_FOLDER)
    pyglet.resource.reindex()
    font.add_directory(RESOURCE_FOLDER)

    director.init( resizable=True, width=1366, height=768 )

    scene = Scene()
    scene.add(TextLayer(), z=2)

    scene.add( MultiplexLayer(
                    MainMenu(),
                    OptionsMenu(),
                    #ScoresLayer(),
                    ),
                z=1 )

    scene.add(BackgroundLayer('bg_title.jpg'), z=0)


    director.window.push_handlers(keyboard)

    fadeable = ColorLayer(255,255,255,255)
    fadeable.do(FadeOut(1))
    scene.add(fadeable, z=2)
    director.run(scene)
